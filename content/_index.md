---
title: "Home Page"
hero:
  hero_link: "#"
  background_image: "turnouts.jpg"
  hero_text: Volunteers needed, turnout today!
  text_color: "#FFFFFF"
  button_text: "Join Us"
stats:
  stats_date: "June 2021"
  stat_medical: 16
  stat_fire: 2
  stat_mva: 1
  stat_other: 9
---
