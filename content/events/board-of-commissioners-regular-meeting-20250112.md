---
title: "Board of Commissioners Regular Meeting "
draft: false
date: 2025-01-14T14:00:00.000Z
---
Where: Main Fire Station on Middlefork Rd in Crouch
When: 14 January 2025 

[https://www.gvfdidaho.com/media/1-14-2025-regular-meeting-agenda.pdf](/media/1-14-2025-regular-meeting-agenda.pdf)
