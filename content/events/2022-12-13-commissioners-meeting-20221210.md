---
title: 2022-12-13 Commissioners Meeting
draft: false
date: 2022-12-13T14:00:27.748Z
---
The GVFPD Fire Commissioners monthly meeting on December 13, 2022 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 13 December 2022, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2022-12-13-regular-meeting-agenda.pdf)