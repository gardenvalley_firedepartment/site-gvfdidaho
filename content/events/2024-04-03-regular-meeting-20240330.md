---
title: 2024-04-03 Regular Meeting
draft: false
date: 2024-04-03T14:00:18.160Z
---
The GVFPD Fire Commissioners meeting is scheduled for April 03 2024 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 03 April, 2024 at 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2024-04-03-regular-meeting-agenda.pdf)