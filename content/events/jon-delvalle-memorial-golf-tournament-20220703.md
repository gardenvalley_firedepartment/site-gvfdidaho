---
title: Jon Delvalle Memorial Golf Tournament
draft: false
date: 2022-07-04T09:00:52.971Z
---
Join us for the 39th annual Garden Valley EMS benefit and silent auction, now known as the Jon Delvalle Memorial Golf Tournament. Proceeds benefit Garden Valley EMS.

<!--more-->

Where: Terrace Lakes (101 Holiday Dr.)

When:

* 8:30 - Golf Cart Parade
* 9:00 - shotgun start and auction begins
* 10:00 - BBQ begins
* 2:00 - auction ends

Did you know...? Your tax dollars fund the **fire** protection district. **EMS** is funded from grants, billing, and donations. Garden Valley EMS receives $0 from tax revenue.