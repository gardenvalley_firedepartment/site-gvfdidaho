---
title: Notice of Budget Hearing
draft: false
date: 2022-08-09T14:00:40.279Z
---
The Garden Valley Fire Protection District Board of Commissioners will hold a Public Hearing regarding the proposed Fiscal Year 2022-23 (“FY 2023”) Budget. The Hearing will be held on the 9th day of August, 2022, at 2PM at the Main Fire Station in Crouch, Idaho. Main Station address is 373 Middle Fork Rd. 

<!--more-->

[Notice of Budget Hearing (pdf)](https://www.gvfdidaho.com/media/fy-2023-released-1p0-idaho-world-notice.pdf)