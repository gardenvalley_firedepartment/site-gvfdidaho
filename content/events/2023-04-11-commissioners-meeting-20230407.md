---
title: 2023-04-11 Commissioner's Meeting
draft: false
date: 2023-04-11T14:00:11.019Z
---
The GVFPD Fire Commissioners monthly meeting on April, 11 2023 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 11 April 2023, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-04-11-commagenda.pdf)