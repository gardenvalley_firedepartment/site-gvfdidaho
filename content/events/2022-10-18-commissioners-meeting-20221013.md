---
title: 2022-10-18 Commissioners Meeting
draft: false
date: 2022-10-18T14:00:00.921Z
---
Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 18 October 2022, 2:00 PM

<!--more-->

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2022-10-18_regularmeetingagenda.pdf)