---
title: 2023-09-12 Regular Commissioner Meeting
draft: false
date: 2023-09-12T14:00:42.532Z
---
The GVFPD Fire Commissioners meeting is scheduled for September 12, 2023 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: September 12  2023, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-09-12-regular-meeting-agenda.pdf)