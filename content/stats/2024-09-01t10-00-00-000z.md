---
title: Monthly Run Stats for {{date | date('YYYY-MM')}}
date: 2024-09-01T10:00:00.000Z
stat_medical: 5
stat_fire: 7
stat_other: 5
---
