---
title: Welcome Chief Cleaveland
date: 2022-08-03T13:38:37.662Z
hero_image: /media/hero-apparatus-command.jpg
hero_link: https://www.gvfdidaho.com/news/2022-08-23-welcome-chief-cleavland/
cta_text: Meet the Chief
cta_text_color: "#fefefe"
cta_align: center
btn_bg_color: "#141212"
btn_text_color: "#ffffff"
cta_btn_text: MEET
---
