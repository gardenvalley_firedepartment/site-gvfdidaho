---
title: Wildland
picture: media/services_wildland.jpg
---
Garden Valley is sits inside the Boise National Forest. 2.5 million acres of grasslands, forests and steep terrain. The GVFD is trained and equipped to fight fires that are remote or WUI (wildland urban interface). Our wildland firefighters pass national testing standards and train to fight and contain wildfires, assist the Forest Service, and protect structures and life.  We have two type 6 wildland engines, two tankers, a draft trailer and can re-purpose our structure rigs to assist as well.