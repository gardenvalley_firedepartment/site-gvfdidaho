---
title: Structure
picture: media/services_structure.jpg
---
With over 2500 homes (and growing) and hundreds of commercial and other buildings, structure fires are an unfortunate reality. From large churches and stores to remote cabins, structure fires are  a serious challenge. The department trains and equips Firefighters to safely battle structure fires using all available resources. We have two fire engines, an attack tanker, and a water tender- all fitted with tools to put the fire out.