---
title: Rescue
picture: media/services_rescue.jpg
---
We much prefer to help rescue a healthy person than to have to retrieve a patient. Many of our members have specialized training in: rope, swiftwater and/or ice rescue and all members are trained to assist in any type of rescue. We work closely with law enforcement, local rafting companies and search and rescue groups to broaden our ability to respond. We have equipment (e.g. UTV and Raft), tools (e.g. Winches and Drones) and clothing (like survival suits) to help find and retrieve people.