---
title: Michael McGrath
active: true
date: 2020-10-11T16:29:24.196Z
badge:
  - cpr
  - emt
  - evoc
  - wildland
  - firefighter
  - hazmat
  - driver
rank: 9. None
layout: volunteer
type: volunteer
image: media/people-volunteers-mike-mcgrath.jpg
---
Mike began volunteering for the Garden Valley Fire District in 2020 because he wants to be of service to the community. He received his EMT license in 2021 and looks forward to serving the community for years to come.  He loves being outdoors and lives here with his beautiful wife.
