---
title: Dean Lange
active: true
date: 2022-06-01T14:23:12.567Z
badge:
  - cpr
  - hazmat
  - firefighter
  - wildland
  - structure
  - evoc
  - rope
  - emt
  - driver
rank: 2. Battalion Chief
layout: officer
type: volunteer
image: /media/dean-lange.jpg
---
After serving four years in the US Navy, Dean spent the next 34 years in the fire service starting as a volunteer. Upon retiring as a Battalion Chief, Dean and his wife Tina moved to Garden Valley in April 2022. The Langes are very excited about being part of the Garden Valley community.
