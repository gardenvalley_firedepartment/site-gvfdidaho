---
title: Paul Cleaveland
active: true
date: 2022-10-01T20:32:12.913Z
badge:
  - paramedic
  - cpr
  - evoc
  - firefighter
  - hazmat
  - structure
  - swiftwater
  - wildland
  - driver
rank: 1. Chief
layout: officer
type: volunteer
image: /media/851-chief-paul-cleaveland.jpg
---
I come from Lansing, Michigan with over thirty years of fire service experience. I served as a Paramedic and a Battalion Chief with a platoon of 45 personnel. I am excited to contribute my talents and years of experience toward GVFPD. I desire to help grow the department as our beloved community continues to grow and thrive.
