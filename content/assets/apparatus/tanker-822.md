---
title: Tanker 822
year: 1992
image: media/assets_apparatus_tanker-822.jpg
type: apparatus
---
Hydrants in garden valley can be counted on one hand. This tanker brings 3500 gallons of  water to the fire.