---
title: Unit 2
year: 2006
image: /media/assets_apparatus_unit-1.jpg
type: apparatus
---
A fully equipped backup ambulance in the event of multiple calls or patients.