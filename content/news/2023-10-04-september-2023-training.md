---
title: September 2023 Training
draft: false
date: 2023-10-04T15:18:16.146Z
---
You may have noticed wrecked cars at Station 2, September training had the whole crew performing stabilization and extrication scenarios.

<!--more-->

Extrication is a complex and potentially dangerous job that needs to be performed quickly. We rehearsed multiple scenarios to ensure we are not only competent but also efficient.

BCSO is often first on-scene to a water rescue. GVFD had a joint training day with BCSO working on swiftwater rescue skills like: safety, rope-bags, and tethered-rescue.

Flu season is around the corner and another COVID spike is possible. EMS reviewed Respiratory Illnesses-this included PPE, protocols, and patient care. 

StateComm gave a fantastic presentation on their systems and protocols. We frequently communicate with StateComm over the radio, but talk is brief and concise. We enjoyed having a relaxed conversation and learning more about StateComm and the many vital tasks they perform.

Finally, we reviewed some new SOGs and equipment assignments.