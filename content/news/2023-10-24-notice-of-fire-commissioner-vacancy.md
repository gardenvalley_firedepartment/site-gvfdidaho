---
title: Notice of Fire Commissioner Vacancy
draft: false
date: 2023-10-24T11:48:53.673Z
---
At this time the Garden Valley Fire Protection District Board of Commissioners is seeking candidates for appointment to the position of Fire Commissioner for GVFPD Sub-district #2.

<!--more-->

[Download Notice (pdf) ](https://www.gvfdidaho.com/media/2023-10-commissioner-vacancy-notice.pdf)