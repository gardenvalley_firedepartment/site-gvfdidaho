---
title: 2023 October Training
draft: false
date: 2023-11-08T06:40:32.543Z
---
October focussed on fire safety and getting new volunteers familiar with fire clothing and equipment.

<!--more-->

Fire reviewed fire exercises, practiced donning and doffing our gear, emergency removal of gear, and SCBA basics. 

EMS trained on equipment, TBIs, splinting, and CPAP as an optional module

We reviewed our Capstan (rope rescue tool) which is now installed on Medic 3. Medic 3 is our last ambulance and also functions as our rescue apparatus.