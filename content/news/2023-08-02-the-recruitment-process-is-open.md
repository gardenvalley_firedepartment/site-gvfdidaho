---
title: The Recruitment Process is Open
draft: false
date: 2023-08-02T16:56:41.745Z
---
Now is a great time to join Garden Valley Fire Department and become 
a professional Firefighter/EMT 

The Recruitment process is now open!! 

Do you have an interest in Helping your community in times of need? 

Garden Valley Fire is a full-service emergency response department providing basic and advanced life support, wildland/structural 
firefighting, high angle rope rescue, and swift water rescue.

Become an emergency medical technician today! State certified EMT class will begin this winter with pre-requisite training starting this Fall and classes will be held here in Garden Valley. 
All other disciplines are held in Garden Valley with training provided.

Compensated On-Call scheduling system allows you to dedicate time without taking away from personal commitments.

Apply Now! Recruitment process will close October 31st, 2023. For more information: call 208-462-3175 or visit the station (373 S Middlefork Rd) Mon-Fri, 9-5.