---
title: Auxiliary
date: 2022-05-19T20:02:58.890Z
image: ""
quicklink: false
---
We help assist with food/logistics support during prolonged incidents, extended training, and other significant events, as well as facilitate community activities related to fire and EMS. The Auxiliary assists in fundraising activities and securing outside sources for equipment, supplies and training for GVFD. The sole purpose of the Auxiliary is to support the GVFD. 

Our meetings are held at the Fire House on the second Monday of every month at 5:30-6:30 pm. We are in need for more volunteers. Please contact Toni Palmiotto 208-315-5418 for more information.

![Auxiliary members with the ambulance](/media/2022-09-14_auxiliary.jpg)