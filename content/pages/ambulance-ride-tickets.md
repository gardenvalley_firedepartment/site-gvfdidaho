---
title: Ambulance Ride Tickets
date: 2022-01-01T14:56:29.744Z
image: media/assets_apparatus_unit-1.jpg
quicklink: true
---
Ride tickets can be purchased at the station for $75 and are good for one ambulance ride to the hospital.
