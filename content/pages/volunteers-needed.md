---
title: Volunteers Needed
date: 2022-04-13T23:17:34.840Z
image: ""
quicklink: true
---
If your neighbor screamed out in distress, would you help?

If you answered yes, you probably have what it takes. When the people in our
community need our help, we answer the call. You can be an everyday hero.

The Garden Valley Fire Protection District is dedicated to safe-guarding lives and
property through education, prevention, and response to all emergency incidents.
The Garden Valley Fire Department responds to medical, fire (structure and wildland),
motor-vehicle accidents and rescue (search, rescue and recovery) operations
throughout the entire 400 square miles of the Garden Valley Fire Protection District.

Firefighter is a catchall term, we need: firefighters, EMTs, drivers, engineers, traffic control and general help on all calls. 

Requirements: desire to help, physically able to perform the tasks, professional , friendly and calm. No experience needed, training provided.

Come by the station (373 S Middlefork Road) M-F, 9-5 to learn more.

https://www.youtube.com/watch?v=kQW1jbnYjN8&authuser=0

![]()
